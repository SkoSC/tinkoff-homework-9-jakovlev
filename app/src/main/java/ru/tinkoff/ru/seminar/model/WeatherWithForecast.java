package ru.tinkoff.ru.seminar.model;

public class WeatherWithForecast {
    private final Weather weather;
    private final WeatherForecast forecast;

    public WeatherWithForecast(Weather weather, WeatherForecast forecast) {
        this.weather = weather;
        this.forecast = forecast;
    }

    public Weather getWeather() {
        return weather;
    }

    public WeatherForecast getForecast() {
        return forecast;
    }
}