package ru.tinkoff.ru.seminar;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;
import ru.tinkoff.ru.seminar.model.Weather;
import ru.tinkoff.ru.seminar.model.WeatherForecast;

public interface WeatherApi {
    @GET("weather")
    Single<Weather> getWeatherForCity(@Query("q") String city);

    @GET("forecast")
    Single<WeatherForecast> getWeatherForecastForCity(@Query("q") String city);
}
