package ru.tinkoff.ru.seminar;

import android.app.Application;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.tinkoff.ru.seminar.adapter.WeatherGsonAdapter;
import ru.tinkoff.ru.seminar.model.Weather;

public class TKFApplication extends Application {
    static final String API_KEY = "7910f4948b3dcb251ebc828f28d8b30b";

    private final Gson gson = makeGson();
    private final WeatherApi weatherApi = makeWeatherApi();

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public WeatherApi getWeatherApi() {
        return weatherApi;
    }

    private Gson makeGson() {
        return new GsonBuilder()
                .registerTypeAdapter(Weather.class, new WeatherGsonAdapter())
                .create();
    }

    private WeatherApi makeWeatherApi() {
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new OpenWeatherApiInterceptor())
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl("http://api.openweathermap.org/data/2.5/")
                .client(client)
                .build();

        return retrofit.create(WeatherApi.class);
    }
}
