package ru.tinkoff.ru.seminar;

import android.annotation.SuppressLint;
import android.support.v7.util.AsyncListUtil;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import ru.tinkoff.ru.seminar.model.Weather;

public class WeatherRecyclerAdapter extends RecyclerView.Adapter<WeatherRecyclerAdapter.ViewHolder> {
    private List<Weather> data = Collections.emptyList();

    void submittItems(List<Weather> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_weather, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Weather model = data.get(position);
        holder.bind(model);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView description;
        private final TextView info;
        private final TextView time;

        public ViewHolder(View itemView) {
            super(itemView);
            description = itemView.findViewById(R.id.weather_description);
            info = itemView.findViewById(R.id.weather_info);
            time = itemView.findViewById(R.id.weather_time);
        }

        @SuppressLint("DefaultLocale")
        void bind(Weather info) {
            this.description.setText(info.description);

            String formattedInfo = String.format("TEMP: %.1f, WIND: %s m/s", info.temp, info.speedWind);
            this.info.setText(formattedInfo);

            Date date = new Date(info.time * 1000L);
            SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
            this.time.setText(sdf.format(date));

        }
    }
}
