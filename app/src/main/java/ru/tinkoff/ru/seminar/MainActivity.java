package ru.tinkoff.ru.seminar;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ru.tinkoff.ru.seminar.model.Weather;
import ru.tinkoff.ru.seminar.model.WeatherForecast;
import ru.tinkoff.ru.seminar.model.WeatherWithForecast;

/**
 * Реализовать приложение, показывающее текущую погоду в городе из предложенного списка.
 * Часть 1. Подготавливаем окружение для взаимодействия с сервером.
 * 1) Сперва получаем ключ для разработчика (Достаточно зарегистрироваться на сайте, он бесплатный) инструкция: https://openweathermap.org/appid
 * <p>
 * 2) Выполнить 2 запроса для получения текущий погоды и прогноза одного из следующих городов:
 * Moscow,RU
 * Sochi,RU
 * Vladivostok,RU
 * Chelyabinsk,RU
 * API запроса By city name можно прочитать тут:
 * https://openweathermap.org/current#name
 * <p>
 * 1) Шаблон запроса на текущую погоду: api.openweathermap.org/data/2.5/weather?q={city name},{country code}
 * Пример: http://api.openweathermap.org/data/2.5/weather?q=Moscow,ru&APPID=7910f4948b3dcb251ebc828f28d8b30b
 * <p>
 * 2) Шаблон запроса на прогноз погоды: api.openweathermap.org/data/2.5/forecast?q={city name},{country code}
 * Пример: http://api.openweathermap.org/data/2.5/forecast?q=Moscow,ru&APPID=7910f4948b3dcb251ebc828f28d8b30b
 * <p>
 * Важно: Данные с сервера должны приходить в json формате (прим.: значение температуры в градусах Цельсия). Также можно добавить локализацию языка: https://openweathermap.org/current#other
 * <p>
 * Часть 2. Разработка мобильного приложения.
 * Шаблон проекта находиться в ветке: homework_9_network
 * UI менять не надо, используем уже реализованные методы MainActivity.
 * Написать код выполнения запроса в методе performRequest(@NonNull String city).
 * <p>
 * Реализовать следующий функционал:
 * a) С помощью Retrofit, Gson и других удобных для вас инструментов, написать запросы для получения текущий и прогнозы погоды в конкретном городе, используя метод API By city name.
 * б) Реализовать JsonDeserializer, который преобразует json структуру пришедшую с сервера в модель Weather (Также и для прогноза погоды). в) Во время загрузки данных показывать прогресс бар, в случае ошибки выводить соотвествующее сообщение.
 * г) Если у пользователя нет доступа в интернет, кнопка выполнить запрос не активна. При его появлении/отсутствии необходимо менять состояние кнопки;
 * д) (Дополнительное задание) Улучшить форматирование вывода данных на свое усмотрение, текущий погоды и прогноза. Оценивается UI интерфейс.
 */

@SuppressWarnings("unused")
public class MainActivity extends AppCompatActivity {
    private static final long NETWORK_CHECKUP_TIME_MILLIS = 200;

    private final CompositeDisposable compDisposable = new CompositeDisposable();
    private ConnectivityManager connectivityManager;

    private Spinner spinner;
    private Button performBtn;
    private ProgressBar progressBar;
    private RecyclerView recycler;
    private WeatherRecyclerAdapter recyclerAdapter;
    private FrameLayout currentWeatherFrame;

    private TextView currentWeatherDescription;
    private TextView currentWeatherInfo;
    private TextView currentWeatherTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        showProgress(false);
        setupPerformButtonUpdates();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        compDisposable.dispose();
    }

    private void init() {
        spinner = findViewById(R.id.spinner);
        performBtn = findViewById(R.id.performBtn);
        progressBar = findViewById(R.id.progressBar);
        recycler = findViewById(R.id.recycler);
        performBtn.setOnClickListener(v -> performRequest(spinner.getSelectedItem().toString()));
        connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        currentWeatherFrame = (FrameLayout) findViewById(R.id.current_weather);

        View view = LayoutInflater.from(this).inflate(R.layout.card_weather, currentWeatherFrame, true);
        currentWeatherDescription = view.findViewById(R.id.weather_description);
        currentWeatherInfo = view.findViewById(R.id.weather_info);
        currentWeatherTime = view.findViewById(R.id.weather_time);

        recycler.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        recyclerAdapter = new WeatherRecyclerAdapter();
        recycler.setAdapter(recyclerAdapter);
    }

    private void setEnablePerformButton(boolean enable) {
        performBtn.setEnabled(enable);
    }

    @SuppressLint("DefaultLocale")
    private void printResult(@NonNull Weather weather, @NonNull List<Weather> forecast) {
        currentWeatherFrame.setVisibility(View.VISIBLE);
        this.currentWeatherDescription.setText(weather.description);

        String formattedInfo = String.format("TEMP: %.1f, WIND: %s m/s", weather.temp, weather.speedWind);
        this.currentWeatherInfo.setText(formattedInfo);

        Date date = new Date(weather.time * 1000L);
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
        this.currentWeatherTime.setText(sdf.format(date));

        recyclerAdapter.submittItems(forecast);
    }

    private void showProgress(boolean visible) {
        progressBar.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
    }

    private void showError(@NonNull String error) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
    }

    private void performRequest(@NonNull String city) {
        showProgress(true);
        Disposable disp = Single
                .zip(makeWeatherRequest(), makeWeatherForecastRequest(), WeatherWithForecast::new)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((resp) -> {
                    showProgress(false);
                    printResult(resp.getWeather(), resp.getForecast().getList());
                }, (err) -> {
                    showProgress(false);
                    showError(err.getMessage());
                });

        compDisposable.add(disp);
    }

    private Single<Weather> makeWeatherRequest() {
        WeatherApi api = getApplicationSafe().getWeatherApi();
        return api.getWeatherForCity(spinner.getSelectedItem().toString());
    }

    private Single<WeatherForecast> makeWeatherForecastRequest() {
        WeatherApi api = getApplicationSafe().getWeatherApi();
        return api.getWeatherForecastForCity(spinner.getSelectedItem().toString());
    }

    private boolean isConnected() {
        return connectivityManager != null
                && connectivityManager.getActiveNetworkInfo() != null
                && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    private void setupPerformButtonUpdates() {
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                setEnablePerformButton(isConnected());
            }
        }, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @NonNull
    private TKFApplication getApplicationSafe() {
        final Application application = getApplication();
        if (application instanceof TKFApplication) {
            return (TKFApplication) application;
        }
        throw new IllegalStateException("Attached application is not instance of TKFApplication");
    }
}

