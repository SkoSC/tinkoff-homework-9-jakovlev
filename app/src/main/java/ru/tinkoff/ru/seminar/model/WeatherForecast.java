package ru.tinkoff.ru.seminar.model;

import java.util.List;

public class WeatherForecast {
    private final List<Weather> list;

    public WeatherForecast(List<Weather> list) {
        this.list = list;
    }

    public List<Weather> getList() {
        return list;
    }
}
