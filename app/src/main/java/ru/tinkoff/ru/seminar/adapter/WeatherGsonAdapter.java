package ru.tinkoff.ru.seminar.adapter;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import ru.tinkoff.ru.seminar.model.Weather;

public class WeatherGsonAdapter implements JsonDeserializer<Weather> {
    private final static String WEATHER_SECTION = "weather";
    private final static String WEATHER_DESCRIPTION = "description";
    private final static String WEATHER_TIME_STAMP = "dt";
    private final static String WEATHER_MAIN = "main";
    private final static String WEATHER_TEMPERATURE = "temp";
    private final static String WEATHER_WIND_OBJ = "wind";
    private final static String WEATHER_WIND_SPEED = "speed";

    @Override
    public Weather deserialize(
            final JsonElement json,
            final Type typeOfT,
            final JsonDeserializationContext context
    ) throws JsonParseException {
        final JsonObject rootObject = json.getAsJsonObject();

        final String description = getDescription(rootObject);
        final long timestamp = getTimeStamp(rootObject);
        final float temp = getTemperature(rootObject);
        final float wind = getWindSpeed(rootObject);

        return new Weather(description, timestamp, temp, wind);
    }

    private String getDescription(JsonObject rootObject) {
        final JsonObject weatherObject = rootObject
                .getAsJsonArray(WEATHER_SECTION)
                .get(0).getAsJsonObject();
        return weatherObject.getAsJsonPrimitive(WEATHER_DESCRIPTION)
                .getAsString();
    }

    private long getTimeStamp(JsonObject rootObject) {
        return rootObject.getAsJsonPrimitive(WEATHER_TIME_STAMP)
                .getAsLong();
    }

    private float getTemperature(JsonObject rootObject) {
        final JsonObject main = rootObject.getAsJsonObject(WEATHER_MAIN);
        return main.getAsJsonPrimitive(WEATHER_TEMPERATURE)
                .getAsFloat() - 273.15F;
    }

    private float getWindSpeed(JsonObject rootObject) {
        final JsonObject main = rootObject.getAsJsonObject(WEATHER_WIND_OBJ);
        return main.getAsJsonPrimitive(WEATHER_WIND_SPEED)
                .getAsFloat();
    }
}
