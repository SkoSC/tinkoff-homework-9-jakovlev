package ru.tinkoff.ru.seminar;

import android.support.annotation.NonNull;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class OpenWeatherApiInterceptor implements Interceptor {
    @NonNull
    @Override
    public Response intercept(@NonNull final Chain chain) throws IOException {
        final Request original = chain.request();
        final HttpUrl originalHttpUrl = original.url();

        final HttpUrl url = originalHttpUrl.newBuilder()
                .addQueryParameter("APPID", TKFApplication.API_KEY)
                .build();

        final Request.Builder requestBuilder = original.newBuilder()
                .url(url);

        final Request request = requestBuilder.build();
        return chain.proceed(request);
    }
}
